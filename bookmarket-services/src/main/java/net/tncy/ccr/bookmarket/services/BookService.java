package net.tncy.ccr.bookmarket.services;

import net.tncy.ccr.bookmarket.data.Book;
import net.tncy.ccr.bookmarket.data.BookFormat;

public class BookService {

	private Book[] books;
	private int lastBookId;

	public BookService() {
		this.books = new Book[0];
		this.lastBookId = -1;
	}

	public int createBook(String title, String author, String publisher, BookFormat format, String isbn) {
		this.lastBookId += 1;
		Book book = new Book(this.lastBookId, title, author, publisher, format, isbn);
		Book[] books = new Book[this.books.length+1];
		for (int i = 0; i < this.books.length; i++)
			books[i] = this.books[i];
		books[books.length-1] = book;
		this.books = books;
		return book.id;
	}

	public void deleteBook(int bookId) {
		int i = 0;
		while (i < this.books.length) {
			if (this.books[i].id == bookId)
				break;
			i += 1;
		}
		if (i < this.books.length) {
			Book[] books = new Book[this.books.length-1];
			for (int j = 0; j < i; j++)
				books[j] = this.books[j];
			for (int j = i; j < books.length; j++)
				books[j] = this.books[j+1];
			this.books = books;
		}
	}

	public Book[] getAllBooks() {
		return this.books.clone();
	}

	public Book getBookById(int bookId) {
		for (int i = 0; i < this.books.length; i++)
			if (this.books[i].id == bookId)
				return this.books[i];
		return null;
	}

	public Book[] findBookByTitle(String title) {
		int count = 0;
		for (int i = 0; i < this.books.length; i++) {
			if (this.books[i].title.equals(title))
				count += 1;
		}
		Book[] books = new Book[count];
		int k = 0;
		for (int i = 0; i < this.books.length; i++) {
			if (this.books[i].title.equals(title)) {
				books[k] = this.books[i];
				k += 1;
			}
		}
		return books;
	}

	public Book[] findBookByAuthor(String author) {
		int count = 0;
		for (int i = 0; i < this.books.length; i++) {
			if (this.books[i].author.equals(author))
				count += 1;
		}
		Book[] books = new Book[count];
		int k = 0;
		for (int i = 0; i < this.books.length; i++) {
			if (this.books[i].author.equals(author)) {
				books[k] = this.books[i];
				k += 1;
			}
		}
		return books;
	}

	public Book getBookByISBN(String isbn) {
		for (int i = 0; i < this.books.length; i++)
			if (this.books[i].isbn.equals(isbn))
				return this.books[i];
		return null;
	}

}
