package net.tncy.ccr.bookmarket.services;

import net.tncy.ccr.bookmarket.data.Book;
import net.tncy.ccr.bookmarket.data.BookFormat;
import net.tncy.ccr.bookmarket.data.Bookstore;
import net.tncy.ccr.bookmarket.data.InventoryEntry;

public class BookstoreService{

	private Bookstore[] bookstores;
	private int lastBookstoreId;

	public BookstoreService() {
		this.bookstores = new Bookstore[0];
		this.lastBookstoreId = -1;
	}

	public int createBookstore(String name) {
		this.lastBookstoreId += 1;
		Bookstore bookstore = new Bookstore(this.lastBookstoreId, name);
		Bookstore[] bookstores = new Bookstore[this.bookstores.length+1];
		for (int i = 0; i < this.bookstores.length; i++)
			bookstores[i] = this.bookstores[i];
		bookstores[bookstores.length-1] = bookstore;
		this.bookstores = bookstores;
		return bookstore.id;
	}

	public void deleteBookstore(int bookstoreId) {
		int i = 0;
		while (i < this.bookstores.length) {
			if (this.bookstores[i].id == bookstoreId)
				break;
			i += 1;
		}
		if (i < this.bookstores.length) {
			Bookstore[] bookstores = new Bookstore[this.bookstores.length-1];
			for (int j = 0; j < i; j++)
				bookstores[j] = this.bookstores[j];
			for (int j = i; j < bookstores.length; j++)
				bookstores[j] = this.bookstores[j+1];
			this.bookstores = bookstores;
		}
	}

	public Bookstore[] getAllBookstores() {
		return this.bookstores.clone();
	}

	public Bookstore getBookstoreById(int bookstoreId) {
		for (int i = 0; i < this.bookstores.length; i++)
			if (this.bookstores[i].id == bookstoreId)
				return this.bookstores[i];
		return null;
	}

	public Bookstore[] findBookstoreByName(String name) {
		int count = 0;
		for (int i = 0; i < this.bookstores.length; i++) {
			if (this.bookstores[i].name.equals(name))
				count += 1;
		}
		Bookstore[] bookstores = new Bookstore[count];
		int k = 0;
		for (int i = 0; i < this.bookstores.length; i++) {
			if (this.bookstores[i].name.equals(name)) {
				bookstores[k] = this.bookstores[i];
				k += 1;
			}
		}
		return bookstores;
	}

	public void renameBookstore(int bookstoreId, String newName) {
		getBookstoreById(bookstoreId).name = newName;
	}

	public void addBookToBookstore(int bookstoreId, Book book, int qty, float price) {
		getBookstoreById(bookstoreId).addBook(book, qty, price);
	}

	public void removeBookFromBookstore(int bookstoreId, int bookId, int qty) {
		getBookstoreById(bookstoreId).removeBook(bookId, qty);
	}

	public InventoryEntry[] getBookstoreInventory(int bookstoreId) {
		Bookstore bookstore = getBookstoreById(bookstoreId);
		return bookstore.inventoryEntries.clone();
	}

}
