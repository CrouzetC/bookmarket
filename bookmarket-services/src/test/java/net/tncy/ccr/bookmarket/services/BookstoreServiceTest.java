package net.tncy.ccr.bookmarket.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import net.tncy.ccr.bookmarket.data.*;
import net.tncy.ccr.bookmarket.services.BookstoreService;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Unit tests for BookstoreService class.
 */
public class BookstoreServiceTest {

    private BookstoreService bookstoreService;

    @Before
    public void init() {
        this.bookstoreService = new BookstoreService();
    }

    @After
    public void clean() {
        this.bookstoreService = null;
    }

    // Test 1
    @Test
    public void testBookstoreService() {

        assertEquals(bookstoreService.getAllBookstores().length, 0);
        
        int bookstore1Id = bookstoreService.createBookstore("Library");
        int bookstore2Id = bookstoreService.createBookstore("Book shop");

        Bookstore[] bookstores = bookstoreService.getAllBookstores();

        assertEquals(bookstores.length, 2);
        assertEquals(bookstores[0].id, bookstore1Id);
        assertEquals(bookstores[1].id, bookstore2Id);
        assertTrue(bookstore1Id != bookstore2Id);
        assertTrue(bookstores[0] != bookstores[1]);
        assertTrue(bookstores[0].name.equals("Library"));
        assertTrue(bookstores[1].name.equals("Book shop"));
        assertEquals(bookstoreService.getBookstoreInventory(bookstore1Id).length, 0);

        Bookstore bookstoreFound1 = bookstoreService.getBookstoreById(bookstore1Id);
        Bookstore bookstoreFound2 = bookstoreService.getBookstoreById(bookstore2Id);

        assertEquals(bookstoreFound1.id, bookstore1Id);
        assertEquals(bookstoreFound2.id, bookstore2Id);

        int bookstore3Id = bookstoreService.createBookstore("Book shop");
        assertEquals(bookstoreService.getAllBookstores().length, 3);

        Bookstore[] bookstoresByName = bookstoreService.findBookstoreByName("Book shop");

        assertEquals(bookstoresByName.length, 2);
        assertTrue(bookstoresByName[0].name.equals("Book shop"));
        assertTrue(bookstoresByName[1].name.equals("Book shop"));

        bookstoreService.deleteBookstore(bookstore3Id);

        assertEquals(bookstoreService.getAllBookstores().length, 2);
        assertEquals(bookstoreService.getAllBookstores()[0].id, bookstore1Id);
        assertEquals(bookstoreService.getAllBookstores()[1].id, bookstore2Id);

        Book book1 = new Book(1, "The Adventure", "Alice", "BookEditor", BookFormat.BROCHE, "1234567890123");
        Book book2 = new Book(2, "The Travel", "Bob", "BookPublisher", BookFormat.POCHE, "1112223334445");

        bookstoreService.addBookToBookstore(bookstore2Id, book1, 1, 13.0f);
        bookstoreService.addBookToBookstore(bookstore2Id, book2, 3, 23.0f);

        assertEquals(bookstoreService.getBookstoreInventory(bookstore1Id).length, 0);
        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id).length, 2);
        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id)[0].book, book1);
        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id)[0].quantity, 1);
        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id)[0].currentPrice, 13.0f, 0.001f);
        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id)[0].averagePrice, 13.0f, 0.001f);
        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id)[1].book, book2);

        bookstoreService.removeBookFromBookstore(bookstore2Id, book2.id, 1);

        assertEquals(bookstoreService.getBookstoreInventory(bookstore1Id).length, 0);
        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id).length, 2);

        bookstoreService.removeBookFromBookstore(bookstore2Id, book1.id, 1);

        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id).length, 1);
        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id)[0].book, book2);

        bookstoreService.removeBookFromBookstore(bookstore2Id, book2.id, 2);

        assertEquals(bookstoreService.getBookstoreInventory(bookstore2Id).length, 0);

        bookstoreService.deleteBookstore(bookstore1Id);

        assertEquals(bookstoreService.getAllBookstores().length, 1);
        assertEquals(bookstoreService.getAllBookstores()[0].id, bookstore2Id);

        bookstoreService.deleteBookstore(bookstore2Id);

        assertEquals(bookstoreService.getAllBookstores().length, 0);
    }

}
