package net.tncy.ccr.bookmarket.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import net.tncy.ccr.bookmarket.data.*;
import net.tncy.ccr.bookmarket.services.BookService;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Unit tests for BookService
 */
public class BookServiceTest {

    private BookService bookService;

    @Before
    public void init() {
        this.bookService = new BookService();
    }

    @After
    public void clean() {
        this.bookService = null;
    }

    // Test 1
    @Test
    public void testBookService() {

        assertEquals(bookService.getAllBooks().length, 0);

        int book1Id = bookService.createBook("The Adventure", "Alice", "BookEditor", BookFormat.BROCHE, "1234567890123");

        assertEquals(bookService.getAllBooks().length, 1);

        int book2Id = bookService.createBook("The Travel", "Bob", "BookPublisher", BookFormat.POCHE, "1112223334445");

        Book[] books = bookService.getAllBooks();
        assertEquals(books.length, 2);
        assertTrue(books[0].id != books[1].id);
        assertTrue(books[0].title.equals("The Adventure"));
        assertTrue(books[1].title.equals("The Travel"));

        Book[] booksTitle = bookService.findBookByTitle("The Travel");
        assertEquals(booksTitle.length, 1);
        assertTrue(booksTitle[0].title.equals("The Travel"));

        Book[] booksAuthor = bookService.findBookByAuthor("Alice");
        assertEquals(booksAuthor.length, 1);
        assertTrue(booksAuthor[0].author.equals("Alice"));
        assertTrue(booksAuthor[0].title.equals("The Adventure"));

        Book bookISBN = bookService.getBookByISBN("1112223334445");
        assertTrue(bookISBN.title.equals("The Travel"));
        assertTrue(bookISBN.isbn.equals("1112223334445"));

        bookService.deleteBook(book1Id);

        assertEquals(bookService.getAllBooks().length, 1);

        bookService.deleteBook(book2Id);

        assertEquals(bookService.getAllBooks().length, 0);
    }

}
