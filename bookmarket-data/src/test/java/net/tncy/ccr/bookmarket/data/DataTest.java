package net.tncy.ccr.bookmarket.data;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import net.tncy.ccr.bookmarket.data.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Unit tests for data classes.
 */
public class DataTest {

    // Testing Book
    @Test
    public void testBook() {
        Book book = new Book(1, "The Adventure", "Alice", "BookEditor", BookFormat.BROCHE, "1234567890123");

        assertEquals(book.id, 1);
        assertTrue(book.title.equals("The Adventure"));
        assertTrue(book.author.equals("Alice"));
        assertTrue(book.publisher.equals("BookEditor"));
        assertEquals(book.format, BookFormat.BROCHE);
        assertTrue(book.isbn.equals("1234567890123"));
    }

    // Testing InventoryEntry
    @Test
    public void testInventoryEntry() {
        Book book = new Book(1, "The Adventure", "Alice", "BookEditor", BookFormat.BROCHE, "1234567890123");
        InventoryEntry inventoryEntry = new InventoryEntry(book, 3, 12.3f, 12.4f);

        assertEquals(inventoryEntry.book, book);
        assertEquals(inventoryEntry.quantity, 3);
        assertTrue(inventoryEntry.averagePrice == 12.3f);
        assertTrue(inventoryEntry.currentPrice == 12.4f);
    }

    // Testing Bookstore
    @Test
    public void testBookstore() {
        Bookstore bookstore = new Bookstore(1, "First storage");

        Book book1 = new Book(1, "The Adventure", "Alice", "BookEditor", BookFormat.BROCHE, "1234567890123");
        Book book2 = new Book(2, "The Travel", "Bob", "BookPublisher", BookFormat.POCHE, "1112223334445");

        assertEquals(bookstore.id, 1);
        assertTrue(bookstore.name.equals("First storage"));
        assertEquals(bookstore.inventoryEntries.length, 0);

        bookstore.addBook(book1,  1, 10.0f);
        bookstore.addBook(book2, 10, 12.0f);

        assertEquals(bookstore.inventoryEntries.length, 2);
        assertEquals(bookstore.getInventoryEntryIndex(book1.id), 0);
        assertEquals(bookstore.getInventoryEntryIndex(book2.id), 1);
        assertEquals(bookstore.inventoryEntries[0].quantity, 1);
        assertEquals(bookstore.inventoryEntries[1].quantity, 10);
        assertTrue(bookstore.inventoryEntries[0].currentPrice == 10.0f);
        assertTrue(bookstore.inventoryEntries[1].currentPrice == 12.0f);

        bookstore.addBook(book1, 1, 20.0f);

        assertEquals(bookstore.inventoryEntries.length, 2);
        assertEquals(bookstore.getInventoryEntryIndex(book1.id), 0);
        assertEquals(bookstore.getInventoryEntryIndex(book2.id), 1);
        assertEquals(bookstore.inventoryEntries[0].quantity, 2);
        assertEquals(bookstore.inventoryEntries[1].quantity, 10);
        assertEquals(bookstore.inventoryEntries[0].currentPrice, 20.0f, 0.001f);
        assertEquals(bookstore.inventoryEntries[0].averagePrice, 15.0f, 0.001f);
        assertEquals(bookstore.inventoryEntries[1].currentPrice, 12.0f, 0.001f);
        assertEquals(bookstore.inventoryEntries[1].averagePrice, 12.0f, 0.001f);

        bookstore.removeBook(book2.id, 5);

        assertEquals(bookstore.inventoryEntries[1].quantity, 5);

        bookstore.removeBook(book1.id, 2);

        assertEquals(bookstore.inventoryEntries.length, 1);
        assertTrue(bookstore.getInventoryEntryIndex(book1.id) < 0);
        assertEquals(bookstore.getInventoryEntryIndex(book2.id), 0);
        assertEquals(bookstore.inventoryEntries[0].book, book2);
        assertEquals(bookstore.inventoryEntries[0].currentPrice, 12.0f, 0.001f);
        assertEquals(bookstore.inventoryEntries[0].averagePrice, 12.0f, 0.001f);

        bookstore.removeBook(book2.id, 5);

        assertEquals(bookstore.inventoryEntries.length, 0);
        assertTrue(bookstore.getInventoryEntryIndex(book1.id) < 0);
        assertTrue(bookstore.getInventoryEntryIndex(book2.id) < 0);
    }

}
