package net.tncy.ccr.bookmarket.data;

public class InventoryEntry {

	public Book book;
	public int quantity;
	public float averagePrice;
	public float currentPrice;

	public InventoryEntry(Book book, int quantity, float averagePrice, float currentPrice) {
		this.book = book;
		this.quantity = quantity;
		this.averagePrice = averagePrice;
		this.currentPrice = currentPrice;
	}

}
