package net.tncy.ccr.bookmarket.data;

public class Bookstore {

	public int id;
	public String name;
	public InventoryEntry[] inventoryEntries;

	public Bookstore(int id, String name) {
		this.id = id;
		this.name = name;
		this.inventoryEntries = new InventoryEntry[0];
	}

	public int getInventoryEntryIndex(int bookId) {
		for (int i = 0; i < this.inventoryEntries.length; i++) {
			if (this.inventoryEntries[i].book.id == bookId) {
				return i;
			}
		}
		return -1;
	}

	public void addBook(Book book, int qty, float price) {
		int iEntry = getInventoryEntryIndex(book.id);
		if (iEntry < 0) {
			int oldLength = this.inventoryEntries.length;
			InventoryEntry[] inventoryEntries = new InventoryEntry[oldLength+1];
			for (int j = 0; j < oldLength; j++)
				inventoryEntries[j] = this.inventoryEntries[j];
			inventoryEntries[oldLength] = new InventoryEntry(book, qty, price, price);
			this.inventoryEntries = inventoryEntries;
		} else {
			InventoryEntry entry = this.inventoryEntries[iEntry];
			entry.averagePrice = ( entry.quantity * entry.averagePrice + qty * price ) / ( entry.quantity + qty );
			entry.quantity += qty;
			entry.currentPrice = price; // we lose the information of the previous books' prices
		}
	}

	public void removeBook(int bookId, int qty) {
		int iEntry = getInventoryEntryIndex(bookId);
		if (iEntry < 0)
			return;
		InventoryEntry entry = this.inventoryEntries[iEntry];
		entry.quantity -= qty;
		if (entry.quantity <= 0) {
			entry.quantity = 0;
			InventoryEntry[] inventoryEntries = new InventoryEntry[this.inventoryEntries.length - 1];
			for (int j = 0; j < iEntry; j++)
				inventoryEntries[j] = this.inventoryEntries[j];
			for (int j = iEntry; j < inventoryEntries.length; j++)
				inventoryEntries[j] = this.inventoryEntries[j+1];
			this.inventoryEntries = inventoryEntries;
		}
	}

}
